import * as Types from './../constants/ActionType';

export const actAddtoCart = (product, quantity) => {
    return {
        type : Types.ADD_TO_CART,
        product,
        quantity
    }
}

export const actChangeMessage = (message) => {  //action thay doi thong bao
    return {
        type : Types.CHANGE_MESSAGE,
        message
    }
}

export const actDeleteProductInCart = (product) => { // action xoa san pham khoi cart
    return {
            type : Types.DELETE_PRODUCT_IN_CART,
            product
    }
}
export const actUpdateProductInCart = (product, quantity) => { 
    return {
            type : Types.UPDATE_PRODUCT_IN_CART,
            product,
            quantity
    }
}