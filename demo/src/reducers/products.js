

var initialState = [
    
        {
        id : 1,
        name : 'iphone',
        image : 'https://store.storeimages.cdn-apple.com/4981/as-images.apple.com/is/image/AppleInc/aos/published/images/i/ph/iphone/x/iphone-x-silver-select-2017?wid=305&hei=358&fmt=jpeg&qlt=95&op_usm=0.5,0.5&.v=1515602510472',
        description : 'san pham Apple',
        price : 33333,
        inventory :10,
        rating :3
        },
        {
            id : 2,
            name : 'samsung',
            image : 'https://store.storeimages.cdn-apple.com/4981/as-images.apple.com/is/image/AppleInc/aos/published/images/i/ph/iphone/x/iphone-x-silver-select-2017?wid=305&hei=358&fmt=jpeg&qlt=95&op_usm=0.5,0.5&.v=1515602510472',
            description : 'san pham samsung',
            price : 88888,
            inventory :10,
            rating :5
        },
        {
            id : 3,
            name : 'Oppo',
            image : 'https://store.storeimages.cdn-apple.com/4981/as-images.apple.com/is/image/AppleInc/aos/published/images/i/ph/iphone/x/iphone-x-silver-select-2017?wid=305&hei=358&fmt=jpeg&qlt=95&op_usm=0.5,0.5&.v=1515602510472',
            description : 'san pham Oppo',
            price : 555555,
            inventory :10,
            rating :4
        }

];

const products = (state = initialState, action) => {
    switch(action.type){
        default : return [...state];
    }
}

export default products;